#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-self-use

from lvfs.pluginloader import PluginBase, PluginSettingBool


class Plugin(PluginBase):
    def __init__(self):
        PluginBase.__init__(self, "wu-copy")
        self.name = "Windows Update"
        self.summary = "Copy files generated using Windows Update"
        self.settings.append(
            PluginSettingBool(key="wu_copy_enable", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSettingBool(
                key="wu_copy_inf", name="Include .inf files", default=True
            )
        )
        self.settings.append(
            PluginSettingBool(
                key="wu_copy_cat", name="Include .cat files", default=True
            )
        )

    def archive_copy(self, cabarchive, cabfile):
        if cabfile.filename.endswith(".inf") and self.get_setting_bool("wu_copy_inf"):
            cabarchive[cabfile.filename] = cabfile
        elif cabfile.filename.endswith(".cat") and self.get_setting_bool("wu_copy_cat"):
            cabarchive[cabfile.filename] = cabfile
