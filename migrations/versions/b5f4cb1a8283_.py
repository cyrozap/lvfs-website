"""

Revision ID: b5f4cb1a8283
Revises: f6771bb55866
Create Date: 2022-05-14 20:30:09.011161

"""

# revision identifiers, used by Alembic.
revision = "b5f4cb1a8283"
down_revision = "f6771bb55866"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "firmware_vendors",
        sa.Column("firmware_vendor_id", sa.Integer(), nullable=False),
        sa.Column("firmware_id", sa.Integer(), nullable=False),
        sa.Column("vendor_id", sa.Integer(), nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("ctime", sa.DateTime(), nullable=False),
        sa.ForeignKeyConstraint(
            ["firmware_id"],
            ["firmware.firmware_id"],
        ),
        sa.ForeignKeyConstraint(
            ["user_id"],
            ["users.user_id"],
        ),
        sa.ForeignKeyConstraint(
            ["vendor_id"],
            ["vendors.vendor_id"],
        ),
        sa.PrimaryKeyConstraint("firmware_vendor_id"),
    )
    op.create_index(
        op.f("ix_firmware_vendors_firmware_id"),
        "firmware_vendors",
        ["firmware_id"],
        unique=False,
    )
    op.create_index(
        op.f("ix_firmware_vendors_vendor_id"),
        "firmware_vendors",
        ["vendor_id"],
        unique=False,
    )


def downgrade():
    op.drop_index(op.f("ix_firmware_vendors_vendor_id"), table_name="firmware_vendors")
    op.drop_index(
        op.f("ix_firmware_vendors_firmware_id"), table_name="firmware_vendors"
    )
    op.drop_table("firmware_vendors")
