"""

Revision ID: d7222878f113
Revises: cb080441ea31
Create Date: 2021-06-30 13:57:02.677206

"""

# revision identifiers, used by Alembic.
revision = "d7222878f113"
down_revision = "cb080441ea31"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("yara_query", sa.Column("percentage", sa.Integer(), nullable=True))


def downgrade():
    op.drop_column("yara_query", "percentage")
