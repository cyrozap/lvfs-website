"""

Revision ID: e7acaaafdf13
Revises: 8f01e8ee583b
Create Date: 2020-12-03 16:34:48.303940

"""

# revision identifiers, used by Alembic.
revision = 'e7acaaafdf13'
down_revision = '8f01e8ee583b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('user_tokens',
    sa.Column('user_token_id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('ctime', sa.DateTime(), nullable=False),
    sa.Column('value', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.user_id'], ),
    sa.PrimaryKeyConstraint('user_token_id')
    )
    op.create_index(op.f('ix_user_tokens_user_id'), 'user_tokens', ['user_id'], unique=False)


def downgrade():
    op.drop_index(op.f('ix_user_tokens_user_id'), table_name='user_tokens')
    op.drop_table('user_tokens')
