"""

Revision ID: 1e3ea6e2d3d0
Revises: 42b4e50aecdb
Create Date: 2022-04-25 11:18:34.311623

"""

# revision identifiers, used by Alembic.
revision = "1e3ea6e2d3d0"
down_revision = "42b4e50aecdb"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_column("components", "release_description")


def downgrade():
    op.add_column(
        "components", sa.Column("release_description", sa.TEXT(), autoincrement=False)
    )
