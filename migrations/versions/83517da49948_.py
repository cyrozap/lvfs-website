"""

Revision ID: 83517da49948
Revises: 09bb942c10ed
Create Date: 2022-03-08 12:27:36.990247

"""

# revision identifiers, used by Alembic.
revision = "83517da49948"
down_revision = "09bb942c10ed"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        "swids",
        sa.Column("swid_id", sa.Integer(), nullable=False),
        sa.Column("component_id", sa.Integer(), nullable=False),
        sa.Column("tag_id", sa.Text(), nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.Column("plugin_id", sa.Text(), nullable=True),
        sa.ForeignKeyConstraint(
            ["component_id"],
            ["components.component_id"],
        ),
        sa.PrimaryKeyConstraint("swid_id"),
    )
    op.create_index(
        op.f("ix_swids_component_id"), "swids", ["component_id"], unique=False
    )
    op.create_index(op.f("ix_swids_tag_id"), "swids", ["tag_id"], unique=False)


def downgrade():
    op.drop_index(op.f("ix_swids_tag_id"), table_name="swids")
    op.drop_index(op.f("ix_swids_component_id"), table_name="swids")
    op.drop_table("swids")
