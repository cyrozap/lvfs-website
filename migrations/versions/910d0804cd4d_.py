"""

Revision ID: 910d0804cd4d
Revises: 950d171bc774
Create Date: 2021-04-15 12:32:39.701217

"""

# revision identifiers, used by Alembic.
revision = '910d0804cd4d'
down_revision = '950d171bc774'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('component_shard_attributes', sa.Column('plugin_id', sa.Text(), nullable=True))


def downgrade():
    op.drop_column('component_shard_attributes', 'plugin_id')
