"""

Revision ID: e8e15b84a9aa
Revises: 47cb29dd15e0
Create Date: 2022-01-24 14:48:45.411861

"""

# revision identifiers, used by Alembic.
revision = "e8e15b84a9aa"
down_revision = "47cb29dd15e0"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("vendors", sa.Column("is_community", sa.Boolean(), nullable=True))


def downgrade():
    op.drop_column("vendors", "is_community")
