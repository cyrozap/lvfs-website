"""

Revision ID: 962f3295b5ea
Revises: 83517da49948
Create Date: 2022-04-12 14:13:30.887260

"""

# revision identifiers, used by Alembic.
revision = "962f3295b5ea"
down_revision = "83517da49948"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column(
        "component_issues", sa.Column("published", sa.DateTime(), nullable=True)
    )


def downgrade():
    op.drop_column("component_issues", "published")
